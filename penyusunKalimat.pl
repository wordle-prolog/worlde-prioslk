% PENYUSUN KALIMAT PETUNJUK

% kalimat(T0,T3,Ind) bernilai true jika T0 dan T3 adalah list kata-kata,
%   di mana T3 merupakan akhir dari T0 dan
%   kata-kata di T0 sebelum T3 (T0-T3) membentuk suatu kalimat.
% Ind adalah kata yang di-refer oleh kalimat.

% Kalimat terdiri dari kata sandang yang diikuti oleh nomina (kata benda),
%   diikuti oleh adjektiva (kata sifat),
%   dan ditambah konjungsi bersama kata sifat lainnya.

kalimat(T0,T3,Ind) :-
    artikula(T0,T1,Ind),
    nomina(T1,T2,Ind),
    adjektiva(T2,T3,Ind).

% Artikula (kata sandang) adalah kata pembuka yang dapat diabaikan.
% Kata sandang tidak menambahkan konteks pada kalimat.

artikula([sebuah| T],T,_).
artikula([suatu | T],T,_).
artikula([para | T],T,_).
artikula(T,T,_).

% Konjungsi (kata hubung) juga tidak diwajibkan.
% Kata hubung hanya untuk menyambungkan adjektiva.

konj([yang | T],T,_).
konj([dan | T],T,_).
konj([atau | T],T,_).
konj([dari | T],T,_).
konj(T,T,_).

% Adjektiva (kata sifat) dapat berisi beberapa sifat yang mendeskripsikan suatu kata.
% Argumen T0, T3, dan Ind memiliki definisi sama seperti pada predikat kalimat.

adjektiva(T0,T3,Ind) :-
    konj(T0,T1,Ind),
    adj(T1,T2,Ind),
    adjektiva(T2,T3,Ind).
adjektiva(T,T,_).

% petunjuk(T0,T1,Ind) bernilai true jika T0-T1 benar untuk Ind.

petunjuk(T0, T1, Ind):-
	kalimat(T0, T1, Ind).


% DICTIONARY

% adj(T0,T1,Ind) akan bernilai true jika T0-T1
%   adalah suatu kata sifat yang mendeskripsikan Ind

adj([manis | T],T,Ind) :- manis(Ind).
adj([asam | T],T,Ind) :- asam(Ind).
adj([Warna | T],T,Ind) :- warna(Ind, Warna).
adj([Iklim | T],T,Ind) :- iklim(Ind, Iklim).
adj([Ukuran | T],T,Ind) :- ukuran(Ind, Ukuran).
adj([Sinonim | T],T,Ind) :- sinonim(Ind, Sinonim).

adj([bukan, Sesuatu | T],T,Ind) :- \+ adj([Sesuatu | T], T, Ind).
adj([tidak, Sesuatu | T],T,Ind) :- \+ adj([Sesuatu | T], T, Ind).

% nomina(T0,T1,Ind) bernilai true jika T0-T1
%   adalah suatu kata benda yang benar mengenai Ind

nomina([buah | T],T,Ind) :- buah(Ind).
nomina([makanan | T], T, Ind) :- makanan(Ind).
nomina([sinonim | T],T,Ind) :- sinonim(Ind, _), !.


% KUMPULAN KATA

% kata(K, List, Length) bernilai true saat kata K memiliki panjang Length
%   dan setiap huruf dari kata tersebut disimpan dalam List

kata(apel, [a,p,e,l], 4).
kata(anggur, [a,n,g,g,u,r], 6).
kata(ceri, [c,e,r,i], 4).
kata(jeruk, [j,e,r,u,k], 5).
kata(lemon, [l,e,m,o,n], 5).
kata(mangga, [m,a,n,g,g,a], 6).
kata(nanas, [n,a,n,a,s], 5).
kata(pir, [p,i,r], 3).
kata(pisang, [p,i,s,a,n,g], 6).
kata(stroberi, [s,t,r,o,b,e,r,i], 8).

kata(akibat, [a,k,i,b,a,t], 6).
kata(amanat, [a,m,a,n,a,t], 6).
kata(bak, [b,a,k], 3).
kata(pungut, [p,u,n,g,u,t], 6).
kata(tabib, [t,a,b,i,b], 5).
kata(unggul, [u,n,g,g,u,l], 6).

