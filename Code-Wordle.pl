% Crossword Puzzle Solver
% Kelompok Wordle

% Muhammad Azhar Hassanuddin - 1806235763
% Primo Giancarlo            - 1806205180
% Reka Paska Enda            - 1806186793

% IMPORT PROLOG FILE

import_pl :-
    source_file(import_pl, Path), % get current file full path
    string_concat(Dir, '/code-wordle.pl', Path), % get current directory\
    
    % path directory for pl file
    string_concat(Dir,'/buatTTS.pl',DirBuatTTS),
    string_concat(Dir,'/penyusunKalimat.pl',DirKalimat),
    string_concat(Dir,'/ttsSolver.pl',DirSolver),
    
    % load all rules and facts from another pl files
    ensure_loaded(DirBuatTTS),
    ensure_loaded(DirKalimat),
    ensure_loaded(DirSolver).

% KARAKTERISTIK

:- dynamic makanan/1, buah/1, manis/1, asam/1, warna/2, ukuran/2, iklim/2, sinonim/2.

% makanan(M) bernilai true jika M adalah makanan.
% buah(B) bernilai true jika B adalah buah.
% manis(M) bernilai true jika M terasa manis.
% asam(A) bernilai true jika A terasa asam.
% warna(Ind, W) bernilai true jika Ind berwarna W.
% ukuran(Ind, U) bernilai true jika Ind berukuran U.
% iklim(Ind, I) bernilai true jika Ind berada di iklim I.
% sinonim(Ind, S) bernilai true jika S merupakan sinonim dari Ind.

import_buah :-
    source_file(import_buah, Path), % get current file full path
    string_concat(Dir, '/code-wordle.pl', Path), % get current directory

    % path directory for all csv file
    string_concat(Dir,'/csv/makanan&buah.csv',DirMakanan),
    string_concat(Dir,'/csv/makanan&buah.csv',DirBuah),
    string_concat(Dir,'/csv/manis.csv',DirManis),
    string_concat(Dir,'/csv/asam.csv',DirAsam),
    string_concat(Dir,'/csv/warna.csv',DirWarna),
    string_concat(Dir,'/csv/ukuran.csv',DirUkuran),
    string_concat(Dir,'/csv/iklim.csv',DirIklim),

    % get csv data and assert it as facts
    csv_read_file(DirMakanan, Makanan, [functor(makanan), separator(0';)]),
    maplist(assert, Makanan),
    csv_read_file(DirBuah, Buah, [functor(buah), separator(0';)]),
    maplist(assert, Buah),
    csv_read_file(DirManis, Manis, [functor(manis), separator(0';)]),
    maplist(assert, Manis),
    csv_read_file(DirAsam, Asam, [functor(asam), separator(0';)]),
    maplist(assert, Asam),
    csv_read_file(DirWarna, Warna, [functor(warna), separator(0';)]),
    maplist(assert, Warna),
    csv_read_file(DirUkuran, Ukuran, [functor(ukuran), separator(0';)]),
    maplist(assert, Ukuran),
    csv_read_file(DirIklim, Iklim, [functor(iklim), separator(0';)]),
    maplist(assert, Iklim).

import_sinonim :-
    source_file(import_sinonim, Path), % get current file full path
    string_concat(Dir, '/code-wordle.pl', Path), % get current directory

    % path directory for all csv file
    string_concat(Dir,'/csv/sinonim.csv',DirSinonim),

    % get csv data and assert it as facts
    csv_read_file(DirSinonim, Sinonim, [functor(sinonim), separator(0';)]),
    maplist(assert, Sinonim).

% Loop utama untuk permainan TTS

start :-
    import_pl,
    game.

game :-
    write('Daftar TTS: '),nl,
    write(' 1 : TTS Buah'),nl,
    write(' 2 : TTS Sinonim'),nl,

    write('Input nomor TTS yang ingin diinginkan: '),
    read(Input1), nl,
    gambar_tts(Input1), nl,

    write('Apa Anda ingin melihat jawaban TTS ini? (y/n): '),
    read(Input2),
    (
        Input2 = y ->
        jawaban_tts(Input1),nl;
        Input2 = n ->
        true;
        write('Salah input, mohon input sesuai ketentuan.'),nl
    ),

    write('Apa Anda ingin melihat TTS lain? (y/n): '),
    read(Input3),
    bersih_bersih,
    (
        Input3 = y ->
        game;
        Input3 = n ->
        write('Bye bye'),nl;
        write('Salah input, mohon input sesuai ketentuan. Bye-bye.'), nl
    ).

% Rules untuk menghapus semua fakta yang diimport

bersih_bersih :-
    retractall(buah(_)),
    retractall(makanan(_)),
    retractall(asam(_)),
    retractall(manis(_)),
    retractall(ukuran(_,_)),
    retractall(iklim(_,_)),
    retractall(warna(_,_)),
    retractall(sinonim(_,_)).

% Rules untuk menggambar tts buah
gambar_tts(1) :-
    write('Gambar TTS Buah'), nl,
    import_buah,
    buatTTSBuah, nl.

% Rules untuk menggambar tts sinonim
gambar_tts(2) :-
    write('Gambar TTS Sinonim'), nl,
    import_sinonim,
    buatTTSSinonim, nl.

% Rules untuk jawaban tts buah
jawaban_tts(1) :-
    write('Jawaban TTS Buah:'), nl,
    tts_buah_dengan_clue(K1, K2, K3, K4, K5, K6, K7),
    write("1. "), write(K1),nl,
    write("2. "), write(K2), write(" (menurun)"),nl,
    write("2. "), write(K7), write(" (mendatar)"),nl,
    write("3. "), write(K3),nl,
    write("4. "), write(K4),nl,
    write("5. "), write(K5),nl,
    write("6. "), write(K6),nl.

% Rules untuk jawaban tts buah
jawaban_tts(2) :-
    write('Jawaban TTS Sinonim:'), nl,
    tts_sinonim(K1, K2, K3, K4, K5, K6),
    write("1. "), write(K1),nl,
    write("2. "), write(K2),nl,
    write("3. "), write(K3),nl,
    write("4. "), write(K4),nl,
    write("5. "), write(K5),nl,
    write("6. "), write(K6),nl.