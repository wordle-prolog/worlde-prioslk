buatTTSBuah :-
    source_file(buatTTSBuah, Path), % get current file full path
    string_concat(Dir, '/buattts.pl', Path), % get current directory
    string_concat(Dir,'/txt/buahclue.txt',DirBuahClue),
    open(DirBuahClue, read, Stream),
    read_file(Stream),
    close(Stream).

buatTTSSinonim :-
    source_file(buatTTSSinonim, Path), % get current file full path
    string_concat(Dir, '/buattts.pl', Path), % get current directory
    string_concat(Dir,'/txt/sinonimclue.txt',DirSinonimClue),
    open(DirSinonimClue, read, Stream),
    read_file(Stream),
    close(Stream).

read_file(Stream) :-
    at_end_of_stream(Stream).

read_file(Stream) :-
    read_line_to_string(Stream, String),
    write(String),nl,
    read_file(Stream).
