% CROSSWORD SOLVER

% titik_potong(K1, K2, I1, I2) bernilai true jika
%   kata K1 berpotongan dengan kata K2
%	pada index I1 untuk K1 dan index I2 untuk W2
%	index dimulai dari 0.

titik_potong(Kata1, Kata2, Index1, Index2):-
	dif(Kata1, Kata2), % memastikan Kata1 dan Kata2 tidak sama
	kata(Kata1, List1, Length1),
	Index1 < Length1,
	nth0(Index1, List1, Huruf), %
	kata(Kata2, List2, Length2),
	Index2 < Length2,
	nth0(Index2, List2, Huruf).


% CONTOH TTS
% K1 = nanas, K2 = pisang, K3 = apel, K4 = lemon, K5 = mangga, K6 = anggur, K7 = pir
% tts_buah_tanpa_clue(K1, K2, K3, K4, K5, K6, K7) :-
%     titik_potong(K1, K2, 4, 2),
%     titik_potong(K2, K7, 0, 0),
%     titik_potong(K2, K3, 3, 0),
%     titik_potong(K3, K4, 3, 0),
%     titik_potong(K4, K5, 2, 0),
%     titik_potong(K4, K6, 4, 1),
%     kata(K1, _, 5),
%     kata(K2, _, 6),
%     kata(K3, _, 4),
%     kata(K4, _, 5),
%     kata(K5, _, 6),
%     kata(K6, _, 6),
%     kata(K7, _, 3).

% K1 = nanas, K2 = pisang, K3 = apel, K4 = lemon, K5 = mangga, K6 = anggur, K7 = pir
tts_buah_dengan_clue(K1, K2, K3, K4, K5, K6, K7) :-
    titik_potong(K1, K2, 4, 2),
    titik_potong(K2, K7, 0, 0),
    titik_potong(K2, K3, 3, 0),
    titik_potong(K3, K4, 3, 0),
    titik_potong(K4, K5, 2, 0),
    titik_potong(K4, K6, 4, 1),
    kata(K1, _, 5),
    kata(K2, _, 6),
    kata(K3, _, 4),
    kata(K4, _, 5),
    kata(K5, _, 6),
    kata(K6, _, 6),
    kata(K7, _, 3),
    petunjuk([suatu, buah, tropis, yang, kuning, dan, tidak, kecil], [], K1),
    petunjuk([suatu, buah, tropis, yang, kuning, dan, sedang], [], K2),
    petunjuk([suatu, buah, merah, yang, sedang], [], K3),
    petunjuk([suatu, buah, kuning, yang, asam], [], K4),
    petunjuk([buah, tropis, hijau, yang, manis], [], K5),
    petunjuk([buah, ungu, yang, kecil], [], K6),
    petunjuk([buah, hijau, yang, sedang, dan, manis], [], K7).

% K1 = pungut, K2 = unggul, K3 = akibat, K4 = amanat, K5 = bak, K6 = tabib
tts_sinonim(K1, K2, K3, K4, K5, K6) :-
    titik_potong(K1, K2, 3, 2),
    titik_potong(K1, K3, 5, 5),
    titik_potong(K3, K4, 4, 0),
    titik_potong(K4, K6, 5, 0),
    titik_potong(K6, K5, 4, 0),
    kata(K1, _, 6),
    kata(K2, _, 6),
    kata(K3, _, 6),
    kata(K4, _, 6),
    kata(K5, _, 3),
    kata(K6, _, 5),
    petunjuk([sinonim, dari, angkat], [], K1),
    petunjuk([sinonim, menang, atau, melebihi], [], K2),
    petunjuk([sinonim, dari, konsekuensi], [], K3),
    petunjuk([sinonim, perintah, dan, pesan], [], K4),
    petunjuk([sinonim, seperti, atau, bagaikan], [], K5),
    petunjuk([sinonim, dari, dukun], [], K6).
